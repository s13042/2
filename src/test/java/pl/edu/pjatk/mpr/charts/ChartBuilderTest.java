 package test.java.pl.edu.pjatk.mpr.charts;

 

import main.java.pl.edu.pjatk.mpr.charts.ChartBuilder;
import main.java.pl.edu.pjatk.mpr.charts.domain.ChartSerie;
import main.java.pl.edu.pjatk.mpr.charts.domain.ChartSettings;
import main.java.pl.edu.pjatk.mpr.charts.domain.ChartType;
import main.java.pl.edu.pjatk.mpr.charts.domain.Point;
import main.java.pl.edu.pjatk.mpr.charts.domain.SerieType;

import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

 








import java.util.ArrayList;
import java.util.List;










import static org.junit.Assert.*;


 public class ChartBuilderTest {

 

    private ChartBuilder chartBuilder;

    @Before
    public void setUp() throws Exception {

        this.chartBuilder = new ChartBuilder();

     }

 

    @After
    public void tearDown() throws Exception {

        this.chartBuilder = null;

     }

 

     @Test
    public void testChartBuilderAddTitle() throws Exception {

        String specificTitle = "Title";

        this.chartBuilder.withTitle(specificTitle);

        Assertions.assertThat(this.chartBuilder)

                .extracting("chartSettings")

                .extracting("title")

                .containsExactly(Tuple.tuple(specificTitle, null));

     }

 

     @Test
    public void testChartBuilderWithLegendAndTitle() throws Exception {

        String title = "title";

        this.chartBuilder.withLegend();

        Assertions.assertThat(this.chartBuilder)

                .extracting("chartSettings")

                .extracting("title", "hasLegend")

                .doesNotContain(Tuple.tuple("",  null))

                .containsExactly(Tuple.tuple(title, true));

 

     }

 

     @Test
    public void testChartBuilderBuildWithChartType() throws Exception {

        ChartSettings chartSettings;

        chartSettings = this.chartBuilder.withType(ChartType.AREA).build();

        Assertions.assertThat(chartSettings)

                .extracting("chartType")

                .hasOnlyElementsOfType(ChartType.class);

 

     }

 

     @Test
    public void testChartBuilderAddSerieExtendIt() throws Exception {

        String[] labels = {"a", "b"};

        ChartSerie serieOne = new ChartSerie();

        serieOne.setSerieType(SerieType.LINE);

        serieOne.setLabel(labels[0]);

        List<Point> listaPoints = new ArrayList<>();
        listaPoints.add(new Point(1, 1));
        serieOne.setPoints(listaPoints);



        this.chartBuilder.addSerie(serieOne);

        Assertions.assertThat(this.chartBuilder.build().getSeries())

                .isNotEmpty()

                .containsExactly(serieOne);



        ChartSerie serieTwo = new ChartSerie();

        serieTwo.setSerieType(SerieType.POINT);

        serieTwo.setLabel(labels[1]);
        List<Point> listaPoint = new ArrayList<>();
        listaPoint.add(new Point(2, 2));
        listaPoint.add(new Point(2, 1));


        serieTwo.setPoints(listaPoint);



        ChartSerie serieEmpty = new ChartSerie();

        serieEmpty.setLabel(labels[2]);

        serieEmpty.setSerieType(SerieType.LINE_POINT);



        List<ChartSerie> chartSeries = new ArrayList<ChartSerie>();

        chartSeries.add(serieTwo);

        chartSeries.add(serieEmpty);



        this.chartBuilder.withSeries(chartSeries);



        Assertions.assertThat(this.chartBuilder.build().getSeries())

                .contains(serieOne, serieEmpty)

                .isNotEmpty()

                .isNotNull()

                .hasSize(3);

     }
    

 }