package test.java.pl.edu.pjatk.mpr.charts;



import main.java.pl.edu.pjatk.mpr.charts.domain.ChartType;

import org.assertj.core.api.Assertions;
import org.junit.Test;




import static org.junit.Assert.*;



public class ChartTypeTest {

    @Test
    public void testChartTypeLineType() throws Exception {

        ChartType chartType = ChartType.LINE;

        Assertions.assertThat(chartType)

                .isEqualTo(ChartType.LINE)

                .isNotEqualTo(ChartType.AREA)

                .isNotNull();



    }

    @Test
    public void testChartTypeLinePointType() throws Exception {

        ChartType chartType = ChartType.LINE_POINT;

        Assertions.assertThat(chartType)

                .isEqualTo(ChartType.LINE_POINT)

                .isNotEqualTo(ChartType.PIE)

                .isNotNull();

    }

}